# AMH Bank

[![pipeline status](https://gitlab.com/angular-material-home/angular-material-home-bank/badges/develop/pipeline.svg)](https://gitlab.com/angular-material-home/angular-material-home-bank/commits/develop)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/f6cfdfe5fa9349da81ad6622c5349296)](https://www.codacy.com/app/angular-material-home/angular-material-home-bank?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/angular-material-home-bank&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/f6cfdfe5fa9349da81ad6622c5349296)](https://www.codacy.com/app/angular-material-home/angular-material-home-bank?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-home/angular-material-home-bank&utm_campaign=Badge_Coverage)



## Document

- [Wiki](https://gitlab.com/angular-material-home-bank/angular-material-home-bank/wikis/home)
- [API document](https://angular-material-home.gitlab.io/angular-material-home-bank/doc/index.html)
