/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeBank')


/**
 * @ngdoc controller
 * @name AmhReceiptCtrl
 * @description manage a receipt
 * 
 */
.controller('AmhReceiptCtrl', function($scope, $routeParams, $bank, $navigator) {

	var ctrl = {
		status: 'relax',
		receipt: null,
		gate: null
	};
	$scope.ctrl = ctrl;

	/**
	 * Sets a receipt in the scope
	 * 
	 * @param receipt
	 */
	function setReceipt(receipt){
		ctrl.receipt = receipt;
		// TODO: set page title
	}

	/**
	 * Get the receipt id
	 * @returns {string} id Id of the recept
	 */
	function getReceiptId() {
		return ($routeParams.id || null);
	}

	/**
	 * Loads receipt data
	 * 
	 */
	function loadReceipt(){
		ctrl.status = 'working';
		//TODO: maso,2019: Check for graphql
		$bank.getReceipt(getReceiptId())//
		.then(function(receipt){
			setReceipt(receipt);
			ctrl.status = 'relax';
			return $bank.getBackend(receipt.backend_id);
		}, function(error){
			ctrl.status = 'fail';
			ctrl.error = error;
		})//
		.then(function(gate){
			ctrl.gate = gate;
		});
	}

	/**
	 * Cancel page
	 */
	function cancel(){
		$navigator.openPage('/');
	}

	$scope.load = loadReceipt;
	loadReceipt();
});
