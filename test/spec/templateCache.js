angular.module('ngMaterialHomeBank').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amh-receipt.html',
    "<md-content class=md-padding layout-padding flex>  <section layout=column layout-align=\"center center\" md-whiteframe=1 layout-margin> <table class=\"receipt-table md-subhead\" ng-show=!status.assetLoading dir=rtl> <tbody> <tr><td translate>id</td><td>{{ctrl.receipt.id}}</td></tr> <tr><td translate>title</td><td>{{ctrl.receipt.title}}</td></tr> <tr><td translate>amount</td><td>{{ctrl.receipt.amount}}</td></tr> <tr><td translate>description</td><td>{{ctrl.receipt.description}}</td></tr> <tr> <td translate>backend</td> <td> <md-icon md-svg-src={{ctrl.gate.symbol}} ng-if=ctrl.gate.symbol></md-icon> {{ctrl.receipt.backend}} - {{ctrl.gate.title}} </td> </tr> <tr> <td translate>status</td> <td translate>{{ctrl.receipt.payRef ? 'payed' : 'not payed'}}</td> </tr> </tbody> </table> <md-button ng-disabled=ctrl.receipt.payRef ng-show=!ctrl.receipt.payRef class=\"md-raised md-primary\" ng-href={{ctrl.receipt.callURL}}>{{'complete pay'| translate}}</md-button> </section> </md-content>"
  );

}]);
